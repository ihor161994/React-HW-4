export const getAuthorsList = (authors, authorsList) => {
	const allAuthors = authors.map((authorID) => {
		const author = authorsList.find((author) => author.id === authorID);
		if (author) {
			return author.name;
		} else return '';
	});

	return allAuthors.join(', ');
};

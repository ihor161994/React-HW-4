export const dateGenerator = () => {
	/*return new Date().toISOString().slice(0, 10).slice(0, 10).replace(/-/g, '/');*/
	return new Date().toLocaleDateString('en-GB', {
		day: 'numeric',
		month: 'numeric',
		year: 'numeric',
	});
};

import { $authHost } from '../../http';
import { addAuthorsData, createAuthorData } from './actionCreators';

export const getAllAuthors = () => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.get('/authors/all');
			dispatch(addAuthorsData(data.result));
		} catch (e) {
			console.log(e?.message);
		}
	};
};

export const addNewAuthor = (newAuthor) => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.post('/authors/add', newAuthor);
			dispatch(createAuthorData(data.result));
		} catch (e) {
			console.log(e?.message);
		}
	};
};

import React, { useState } from 'react';

import Input from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';

import styles from './Login.module.scss';
import { useDispatch } from 'react-redux';
import { login } from '../../http/services';

const Login = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const [currentUser, setCurrentUser] = useState({
		password: '',
		email: '',
	});

	const signIn = async (e) => {
		e.preventDefault();
		dispatch(login(currentUser));
		navigate('/');
	};

	return (
		<div className={'container'}>
			<div className={styles.login}>
				<h3>Login</h3>
				<form onSubmit={(e) => signIn(e)}>
					<Input
						placeholderText={'Enter email'}
						labelText={'Email'}
						onChange={(e) =>
							setCurrentUser({ ...currentUser, email: e.target.value })
						}
						value={currentUser.email}
						type={'email'}
					/>
					<Input
						placeholderText={'Enter password'}
						labelText={'Password'}
						onChange={(e) =>
							setCurrentUser({ ...currentUser, password: e.target.value })
						}
						value={currentUser.password}
						type={'password'}
					/>
					<Button text={'Login'} />
				</form>
				<p>
					If you not have an account you can{' '}
					<Link to='/registration' className={styles.registration}>
						Registration
					</Link>
				</p>
			</div>
		</div>
	);
};

export default Login;

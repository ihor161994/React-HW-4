import React, { useState } from 'react';

import { useNavigate, useParams } from 'react-router-dom';

import Input from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';

import { pipeDurationTime } from '../../helpers/pipeDuration';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRotateRight } from '@fortawesome/free-solid-svg-icons';

import styles from './CourseForm.module.scss';
import { useDispatch, useSelector } from 'react-redux';

import { getAuthors, getCourses } from '../../store/selectors';
import { addNewAuthor } from '../../store/authors/thunk';
import { addCourse, updateCourse } from '../../store/courses/thunk';

const CourseForm = () => {
	const { courseId } = useParams();
	const coursesList = useSelector(getCourses);
	const authorsList = useSelector(getAuthors);
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const [newAuthor, setNewAuthor] = useState('');

	const courseInitialState = {
		title: '',
		description: '',
		duration: 0,
		authors: [],
	};

	const initialStateCourse = () => {
		if (courseId) {
			return coursesList.filter((course) => course.id === courseId)[0];
		} else return courseInitialState;
	};

	const [course, setCourse] = useState(initialStateCourse);

	const authorsListFiltered = () => {
		return authorsList.filter((author) => {
			return !course.authors.includes(author.id);
		});
	};

	const coursesListFiltered = () => {
		return authorsList.filter((author) => {
			return course.authors.includes(author.id);
		});
	};

	const handleAddNewAuthor = () => {
		dispatch(addNewAuthor({ name: newAuthor }));
	};

	const addAuthor = (id) => {
		setCourse({ ...course, authors: [...course.authors, id] });
	};
	const deleteAuthor = (id) => {
		setCourse({
			...course,
			authors: course.authors.filter((authorId) => authorId !== id),
		});
	};

	const createCourse = () => {
		if (
			course.title.length < 3 ||
			course.title.description < 5 ||
			course.title.duration < 1 ||
			course.title.authors < 1
		) {
			alert('fill in all fields');
		} else {
			courseId
				? dispatch(updateCourse(courseId, course))
				: dispatch(addCourse(course));

			navigate('/');
		}
	};

	return (
		<div className={'container'}>
			<div className={styles.createCourse}>
				<div className={styles.title}>
					<div className={styles.titleInput}>
						<Input
							placeholderText={'Enter title...'}
							labelText={'Title'}
							value={course.title}
							onChange={(e) => setCourse({ ...course, title: e.target.value })}
						/>
					</div>
					<Button
						text={courseId ? 'Update course' : 'Create course'}
						onClick={createCourse}
					/>
				</div>
				<div className={styles.description}>
					<label htmlFor='description'>Description</label>
					<textarea
						value={course.description}
						id='description'
						placeholder={'Enter description...'}
						onChange={(e) =>
							setCourse({ ...course, description: e.target.value })
						}
					/>
					<FontAwesomeIcon
						onClick={() => setCourse({ ...course, description: '' })}
						className={styles.icon}
						icon={faArrowRotateRight}
					/>
				</div>
				<div className={styles.authorForm}>
					<div className={styles.authorFormContainer}>
						<h3>Add author</h3>
						<Input
							placeholderText={'Enter author name...'}
							labelText={'Author name'}
							onChange={(e) => setNewAuthor(e.target.value)}
						/>
						<Button text={'Create author'} onClick={handleAddNewAuthor} />
						<h3>Duration</h3>
						<Input
							type='number'
							placeholderText={'Enter duration in minutes...'}
							labelText={'Duration'}
							value={course.duration}
							onChange={(e) =>
								setCourse({ ...course, duration: Number(e.target.value) })
							}
						/>
						<p className={styles.durationHours}>
							Duration: {pipeDurationTime(course.duration)}
						</p>
					</div>
					<div className={styles.authorFormContainer}>
						<h3>Authors</h3>
						{authorsListFiltered().length ? (
							authorsListFiltered().map((author) => (
								<div key={author.id} className={styles.addAuthorContainer}>
									<p>{author.name}</p>
									<Button
										text={'Add author'}
										onClick={() => addAuthor(author.id)}
									/>
								</div>
							))
						) : (
							<p>Author list is empty</p>
						)}
						<h3>Course authors</h3>
						{coursesListFiltered().length ? (
							coursesListFiltered().map((author) => (
								<div key={author.id} className={styles.addAuthorContainer}>
									<p>{author.name}</p>
									<Button
										text={'Delete author'}
										onClick={() => deleteAuthor(author.id)}
									/>
								</div>
							))
						) : (
							<p>Author list is empty</p>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseForm;

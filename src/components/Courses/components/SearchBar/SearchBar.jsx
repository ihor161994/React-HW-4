import React from 'react';

import Input from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';

import styles from './SearchBar.module.scss';

const SearchBar = ({ searchQuery, setSearchQuery, getSortedCourses }) => {
	return (
		<div className={styles.searchBar}>
			<Input
				placeholderText={'Enter course name'}
				labelText={''}
				onChange={(e) => setSearchQuery(e.target.value)}
				value={searchQuery}
			/>
			<Button text={'Search'} onClick={getSortedCourses} />
		</div>
	);
};

export default SearchBar;

import React from 'react';
import styles from './CourseCard.module.scss';
import { Button } from '../../../../common/Button/Button';
import {
	pipeDurationDate,
	pipeDurationTime,
} from '../../../../helpers/pipeDuration';
import { useNavigate } from 'react-router-dom';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCourseData } from '../../../../store/courses/actionCreators';
import { getUser } from '../../../../store/selectors';
import { deleteCourse } from '../../../../store/courses/thunk';
const CourseCard = ({
	data: { title, description, creationDate, duration, id },
	authors,
}) => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const user = useSelector(getUser);

	return (
		<div className={styles.card}>
			<div className={styles.contentContainer}>
				<h3>{title}</h3>
				<p>{description}</p>
			</div>
			<div className={styles.infoContainer}>
				<p>
					Authors:<span>{authors}</span>
				</p>
				<p>
					Duration:<span>{pipeDurationTime(duration)}</span>
				</p>
				<p>
					Created:<span>{pipeDurationDate(creationDate)}</span>
				</p>
				<div className={styles.tools}>
					<Button
						text={'Show course'}
						onClick={() => navigate(`/courses/${id}`)}
					/>

					{user.role === 'admin' && (
						<>
							<Button
								text={<FontAwesomeIcon icon={faPen} />}
								onClick={() => navigate(`/courses/update/${id}`)}
							/>
							<Button
								text={<FontAwesomeIcon icon={faTrash} />}
								onClick={() => {
									dispatch(deleteCourse(id));
								}}
							/>
						</>
					)}
				</div>
			</div>
		</div>
	);
};

export default CourseCard;

import React, { useEffect, useState } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';

import { getAuthorsList } from '../../helpers/getAuthorsList';

import styles from './Courses.module.scss';
import { useSelector } from 'react-redux';
import { coursesFilter } from '../../helpers/coursesFilter';
import { getAuthors, getCourses } from '../../store/selectors';

const Courses = () => {
	const authorsList = useSelector(getAuthors);
	const coursesList = useSelector(getCourses);

	const navigate = useNavigate();
	const handelGoToNewCourse = () => {
		return navigate('/courses/add');
	};

	const [searchQuery, setSearchQuery] = useState('');
	const [sortedCourses, setSortedCourses] = useState(coursesList);

	const getSortedCourses = () => {
		if (searchQuery) {
			setSortedCourses(coursesFilter([...coursesList], searchQuery));
		} else {
			setSortedCourses(coursesList);
		}
	};

	useEffect(() => {
		if (searchQuery.length < 1) {
			setSortedCourses(coursesList);
		}
	}, [searchQuery, coursesList]);

	return (
		<div className={'container'}>
			<div className={styles.courses}>
				<div className={styles.bar}>
					<SearchBar
						searchQuery={searchQuery}
						setSearchQuery={setSearchQuery}
						getSortedCourses={getSortedCourses}
					/>
					<Button text={'Add new course'} onClick={handelGoToNewCourse} />
				</div>
				{sortedCourses.map((card) => (
					<CourseCard
						data={card}
						authors={getAuthorsList(card.authors, authorsList)}
						key={card.id}
					/>
				))}
			</div>
		</div>
	);
};

export default Courses;

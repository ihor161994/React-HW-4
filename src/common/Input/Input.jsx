import React from 'react';
import styles from './Input.module.scss';
import { v4 as randomId } from 'uuid';

const Input = ({ placeholderText, labelText, onChange, ...props }) => {
	const id = randomId();

	return (
		<div className={styles.container}>
			<label htmlFor={id}>{labelText}</label>
			<input
				type='text'
				id={id}
				placeholder={placeholderText}
				onChange={onChange}
				{...props}
			/>
		</div>
	);
};

export default Input;
